using System.Collections;
using System.Collections.Generic;
using static CustomEquipSlotType;

public class ItemTemplate {
  public string baseItemReference;
  public string uniqueName;
  public string displayName;
  public int uniqueIndex;
  public string description;
  public int maxUses;
  public int maxStacks;
  public EquipSlotType equipSlotType;
  public string subCategory;
  public int subCategoryOrder;
  public Dictionary<string, int> recipe;
  public Dictionary<string, float> modifiers;
  public string spriteName;

  public ItemTemplate(string baseItemReference, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, int maxStacks, EquipSlotType equipSlotType, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, Dictionary<string, float> modifiers, string spriteName = null) {
    this.baseItemReference = baseItemReference;
    this.uniqueName = uniqueName;
    this.displayName = displayName;
    this.uniqueIndex = uniqueIndex;
    this.description = description;
    this.maxUses = maxUses;
    this.maxStacks = maxStacks;
    this.equipSlotType = equipSlotType;
    this.subCategory = subCategory;
    this.subCategoryOrder = subCategoryOrder;
    this.recipe = recipe;
    this.modifiers = modifiers;
    this.spriteName = spriteName;
  }
}

public static class AugmentedEquipmentItems {
  public static Dictionary<string, ItemTemplate> AllAugmentedEquipmentItems = new Dictionary<string, ItemTemplate> {
    ["Scrap_Pulley"] = new ItemTemplate(
      "Equipment_LeatherChest",
      "Scrap_Pulley",
      "Scrap Pulley",
      16400,
      "An rudimentary hydraulic arm brace that helps the wearer pull with more force when using a Scrap Hook.",
      300,
      1,
      CustomEquipSlotType.Get("Chest"),
      "Pulley",
      0,
      new Dictionary<string, int> { ["Scrap"] = 12, ["Bolt"] = 2, ["VineGoo"] = 2, ["Rope"] = 4 },
      new Dictionary<string, float> {
        ["pullSpeedModifier"] = 0.15f,
        ["gatherTimeModifier"] = 0.25f
      }
    ),
    ["Titanium_Pulley"] = new ItemTemplate(
      "Equipment_LeatherChest",
      "Titanium_Pulley",
      "Titanium Pulley",
      16401,
      "An advanced hydraulic arm brace that helps the wearer pull with great force when using a Scrap Hook.",
      900,
      1,
      CustomEquipSlotType.Get("Chest"),
      "Pulley",
      1,
      new Dictionary<string, int> { ["TitaniumIngot"] = 4, ["Hinge"] = 4, ["Bolt"] = 6, ["Scrap"] = 20, ["Rope"] = 8 },
      new Dictionary<string, float> {
        ["pullSpeedModifier"] = 0.35f,
        ["gatherTimeModifier"] = 0.5f
      }
    ),
    ["Rugged_Boots"] = new ItemTemplate(
      "Equipment_LeatherLegs",
      "Rugged_Boots",
      "Rugged Boots",
      16402,
      "A pair of rugged boots to make moving around more comfortable and easy.",
      35,
      1,
      CustomEquipSlotType.Get("Feet"),
      "CustomArmorFeet",
      0,
      new Dictionary<string, int> { ["Bolt"] = 4, ["Scrap"] = 10, ["Leather"] = 8, ["Wool"] = 4, ["Rope"] = 2, ["VineGoo"] = 2 },
      new Dictionary<string, float> {
        ["normalSpeedModifier"] = 0.35f,
        ["sprintSpeedModifier"] = 0.35f
      }
    ),
    ["Exploration_Boots"] = new ItemTemplate(
      "Equipment_LeatherLegs",
      "Exploration_Boots",
      "Exploration Boots",
      16403,
      "A pair of exploration boots -- the most optimal pair of footwear to get around!",
      55,
      1,
      CustomEquipSlotType.Get("Feet"),
      "CustomArmorFeet",
      1,
      new Dictionary<string, int> { ["TitaniumIngot"] = 7, ["Bolt"] = 8, ["Scrap"] = 20, ["Leather"] = 16, ["Wool"] = 10, ["Rope"] = 4, ["VineGoo"] = 8 },
      new Dictionary<string, float> {
        ["normalSpeedModifier"] = 0.65f,
        ["sprintSpeedModifier"] = 0.65f
      }
    ),
    ["Reinforced_Flippers"] = new ItemTemplate(
      "Flipper",
      "Reinforced_Flippers",
      "Reinforced Flippers",
      16404,
      "A pair of flippers reinforced with more durable parts that generate more thrust.",
      1500,
      1,
      CustomEquipSlotType.Get("SwimFeet"),
      "CustomArmorFeet",
      2,
      new Dictionary<string, int> { ["Flipper"] = 1, ["Hinge"] = 4, ["Scrap"] = 20, ["VineGoo"] = 6, ["SeaVine"] = 8, ["Rope"] = 4 },
      new Dictionary<string, float> {
        ["swimSpeedModifier"] = 0.9f
      }
    ),
    ["Carbon_Steel_Flippers"] = new ItemTemplate(
      "Flipper",
      "Carbon_Steel_Flippers",
      "Carbon Steel Flippers",
      16405,
      "A pair of ultra lightweight titanium alloy flippers that greatly enhance swimming capabilities.",
      3500,
      1,
      CustomEquipSlotType.Get("SwimFeet"),
      "CustomArmorFeet",
      3,
      new Dictionary<string, int> { ["Reinforced_Flippers"] = 1, ["TitaniumIngot"] = 4, ["Hinge"] = 8, ["Bolt"] = 4, ["VineGoo"] = 8 },
      new Dictionary<string, float> {
        ["swimSpeedModifier"] = 1.6f,
      }
    ),
    ["Worn_Gloves"] = new ItemTemplate(
      "ZiplineTool",
      "Worn_Gloves",
      "Worn Gloves",
      16406,
      "A pair of worn gloves that provides grip to more safely swing and cut.",
      300,
      1,
      CustomEquipSlotType.Get("Glove"),
      "CustomArmorGlove",
      0,
      new Dictionary<string, int> { ["Leather"] = 4, ["Thatch"] = 20, ["Rope"] = 8 },
      new Dictionary<string, float> {
        ["axeButtonCooldownModifier"] = 0.25f,
        ["gatherSpeedModifier"] = 0.25f
      }
    ),
    ["Grip_Gloves"] = new ItemTemplate(
      "ZiplineTool",
      "Grip_Gloves",
      "Grip Gloves",
      16407,
      "A pair of work gloves that are comfortable and grippy with better materials and reinforcement.",
      900,
      1,
      CustomEquipSlotType.Get("Glove"),
      "CustomArmorGlove",
      1,
      new Dictionary<string, int> { ["Worn_Gloves"] = 1, ["Leather"] = 8, ["Rope"] = 12, ["Wool"] = 4, ["Scrap"] = 20, ["Bolt"] = 6 },
      new Dictionary<string, float> {
        ["axeButtonCooldownModifier"] = 0.45f,
        ["gatherSpeedModifier"] = 0.45f
      }
    ),
    ["Duffel_Backpack"] = new ItemTemplate(
      "Backpack",
      "Duffel_Backpack",
      "Duffel Backpack",
      16408,
      "A backpack that has been torn apart and upgraded to a larger duffel backpack.",
      99999,
      1,
      CustomEquipSlotType.Get("Backpack"),
      "CustomArmorBackpack",
      0,
      new Dictionary<string, int> { ["Backpack"] = 1, ["Scrap"] = 40, ["Hinge"] = 15, ["Bolt"] = 15, ["Leather"] = 20, ["Wool"] = 10 },
      null
    ),
    ["Tactical_Backpack"] = new ItemTemplate(
      "Backpack",
      "Tactical_Backpack",
      "Tactical Backpack",
      16409,
      "A duffel backpack that has been torn apart and upgraded to a tactical backpack.",
      99999,
      1,
      CustomEquipSlotType.Get("Backpack"),
      "CustomArmorBackpack",
      1,
      new Dictionary<string, int> { ["Duffel_Backpack"] = 1, ["Scrap"] = 60, ["Hinge"] = 30, ["Bolt"] = 30, ["Leather"] = 30, ["Wool"] = 15 },
      null
    ),
    ["Equipment_LeatherLegs"] = new ItemTemplate(
      null, null, null, 0, null, 0, 1, CustomEquipSlotType.Get("Feet"), null, 0, null,
      new Dictionary<string, float> {
        ["normalSpeedModifier"] = 0.15f,
        ["sprintSpeedModifier"] = 0.15f
      }
    ),
    ["Backpack"] = new ItemTemplate(
      null, null, null, 0, null, 0, 1, CustomEquipSlotType.Get("Backpack"), null, 0, null, null
    ),
    ["OxygenBottle"] = new ItemTemplate(
      null, null, null, 0, null, 0, 1, CustomEquipSlotType.Get("Head"), null, 0, null,
      new Dictionary<string, float> {
        ["oxygenLossModifier"] = 0.4f
      }
    ),
    ["Oxygen_Canister"] = new ItemTemplate(
      "OxygenBottle",
      "Oxygen_Canister",
      "Oxygen Canister",
      16410,
      "An oxygen canister made out of oxygen bottles and spare metal pieces to handle greater pressure.",
      1200,
      1,
      CustomEquipSlotType.Get("Head"),
      "CustomHead",
      0,
      new Dictionary<string, int> { ["OxygenBottle"] = 2, ["VineGoo"] = 8, ["MetalIngot"] = 4, ["CopperIngot"] = 4, ["Scrap"] = 10 },
      new Dictionary<string, float> {
        ["oxygenLossModifier"] = 0.55f
      }
    ),
    ["Oxygen_Tank"] = new ItemTemplate(
      "OxygenBottle",
      "Oxygen_Tank",
      "Oxygen Tank",
      16411,
      "An oxygen tank that is made from a pair of canisters to provide large amount of oxygen while being incredibly durable.",
      8000,
      1,
      CustomEquipSlotType.Get("Head"),
      "CustomHead",
      1,
      new Dictionary<string, int> { ["Oxygen_Canister"] = 2, ["VineGoo"] = 4, ["MetalIngot"] = 10, ["CopperIngot"] = 10, ["Scrap"] = 20 },
      new Dictionary<string, float> {
        ["oxygenLossModifier"] = 0.7f
      }
    ),
    ["Flipper"] = new ItemTemplate(
      null, null, null, 0, null, 0, 1, CustomEquipSlotType.Get("SwimFeet"), null, 0, null,
      new Dictionary<string, float> {
        ["swimSpeedModifier"] = 0.4f
      }
    ),
    ["Thatch_Belt"] = new ItemTemplate(
      "ZiplineTool",
      "Thatch_Belt",
      "Thatch Belt",
      16412,
      "It's not much but at least this belt provides some utility -- assuming it doesn't just break apart somehow.",
      1,
      1,
      CustomEquipSlotType.Get("Belt"),
      "CustomArmorBelt",
      0,
      new Dictionary<string, int> { ["Rope"] = 10, ["Thatch"] = 40, ["Scrap"] = 5 },
      null
    ),
    ["Tattered_Belt"] = new ItemTemplate(
      "ZiplineTool",
      "Tattered_Belt",
      "Tattered Belt",
      16413,
      "With whatever bits and pieces here and there, you managed to make a belt that won't just disintegrate.",
      1,
      1,
      CustomEquipSlotType.Get("Belt"),
      "CustomArmorBelt",
      1,
      new Dictionary<string, int> { ["Thatch_Belt"] = 1, ["Rope"] = 20, ["MetalIngot"] = 2, ["CopperIngot"] = 1, ["Nail"] = 20 },
      null
    ),
    ["Leather_Belt"] = new ItemTemplate(
      "ZiplineTool",
      "Leather_Belt",
      "Leather Belt",
      16414,
      "With some quality leather, you now have your hands on a more functional belt.",
      1,
      1,
      CustomEquipSlotType.Get("Belt"),
      "CustomArmorBelt",
      1,
      new Dictionary<string, int> { ["Tattered_Belt"] = 1, ["Rope"] = 20, ["Leather"] = 20, ["MetalIngot"] = 5, ["Nail"] = 20 },
      null
    ),
    ["Utility_Belt"] = new ItemTemplate(
      "ZiplineTool",
      "Utility_Belt",
      "Utility Belt",
      16415,
      "Using overall higher quality materials, your belt is significantly more sturdy while being upholstered to give you more flexibility.",
      1,
      1,
      CustomEquipSlotType.Get("Belt"),
      "CustomArmorBelt",
      1,
      new Dictionary<string, int> { ["Leather_Belt"] = 1, ["Rope"] = 30, ["Leather"] = 20, ["MetalIngot"] = 20, ["CopperIngot"] = 10, ["Nail"] = 40 },
      null
    ),
    ["Caravan_Belt"] = new ItemTemplate(
      "ZiplineTool",
      "Caravan_Belt",
      "Caravan Belt",
      16416,
      "From careful notes and blueprints found from the Caravan, you've created the most functional belt yet!",
      1,
      1,
      CustomEquipSlotType.Get("Belt"),
      "CustomArmorBelt",
      1,
      new Dictionary<string, int> { ["Utility_Belt"] = 1, ["Rope"] = 40, ["Leather"] = 30, ["TitaniumIngot"] = 10, ["MetalIngot"] = 20, ["CopperIngot"] = 20 },
      null
    ),
    ["Fortified_Gloves"] = new ItemTemplate(
      "ZiplineTool",
      "Fortified_Gloves",
      "Fortified Gloves",
      16417,
      "A pair of gloves made from the most comfortable and sturdy material you could find.",
      2100,
      1,
      CustomEquipSlotType.Get("Glove"),
      "CustomArmorGlove",
      2,
      new Dictionary<string, int> { ["Grip_Gloves"] = 1, ["Leather"] = 8, ["Wool"] = 8, ["TitaniumIngot"] = 2, ["MetalIngot"] = 6, ["Bolt"] = 6 },
      new Dictionary<string, float> {
        ["axeButtonCooldownModifier"] = 0.60f,
        ["gatherSpeedModifier"] = 0.60f
      }
    ),
  };

  public static ItemTemplate Get(string uniqueName) {
    return AllAugmentedEquipmentItems[uniqueName];
  }
}