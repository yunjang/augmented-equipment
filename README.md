<img width="660" height="200" src="banner.png">

---

### Tested for Multiplayer! All players must have the mods installed and enabled.

---

### Last Updated - 11/23/2020

### Latest Version Tested - 12.01

### Current Mod Version - 1.3.2

---

## Summary
This mod adds in additional equippable items that work in similar fashion to existing ones. Each of the equipment augments the player's properties such as the ability to pull in the hook faster, gather items underwater faster, move faster, etc.

For the sake of balance, the items inherit similar properties to existing items such as the Leather armor pieces. This means they have their respective body part slots they get equipped to and will unequip if the respective body part slot is occupied. Additionally, all the items take durability damage based on their respective actions -- so you will still have to re-craft them as you go (worry not though -- the durability is better scaled to not be a complete chore). Additionally, the cost of many of these items will be high as it's intended to act as a resource sink.

Please note that the mods do end up making you rather "overpowered" in the sense of making things less of a chore. As a result, the augmented effects may change from version to version.

---

## Augments

| Item Icon | Item Name | Body Part | Durability | Property Augments |
| :---: | :---: | :---: | :---: | :---: |
| <img width="64" height="64" src="Assets/Scrap_Pulley.png"> | Scrap Pulley | Chest | 300 | +15% Pull Speed / -25% Gather Time |
| <img width="64" height="64" src="Assets/Titanium_Pulley.png"> | Titanium Pulley | Chest | 900 |+35% Pull Speed / -50% Gather Time |
| <img width="64" height="64" src="https://static.wikia.nocookie.net/raft_gamepedia_en/images/7/72/Leather_Greaves.png/revision/latest?cb=20201014150529"> | Leather Boots | Legs | 25 | +15% Normal Speed / +15% Sprint Speed |
| <img width="64" height="64" src="Assets/Rugged_Boots.png"> | Rugged Boots | Feet | 35 | +35% Normal Speed / +35% Sprint Speed |
| <img width="64" height="64" src="Assets/Exploration_Boots.png"> | Exploration Boots | Feet | 55 | +65% Normal Speed / +65% Sprint Speed |
| <img width="64" height="64" src="Assets/Reinforced_Flippers.png"> | Reinforced Flippers | Swim Feet | 1500 | +90% Swim Speed |
| <img width="64" height="64" src="Assets/Carbon_Steel_Flippers.png"> | Carbon Steel Flippers | Swim Feet | 3500 | +160% Swim Speed |
| <img width="64" height="64" src="Assets/Worn_Gloves.png"> | Worn Gloves | Gloves | 300 | +25% Axe Swing Speed / +25% Item Gathering Speed |
| <img width="64" height="64" src="Assets/Grip_Gloves.png"> | Grip Gloves | Gloves | 900 | +45% Axe Swing Speed / +45% Item Gathering Speed |
| <img width="64" height="64" src="Assets/Fortified_Gloves.png"> | Grip Gloves | Gloves | 2100 | +60% Axe Swing Speed / +60% Item Gathering Speed |
| <img width="64" height="64" src="Assets/Oxygen_Canister.png"> | Oxygen Canister | Head | 1200 | -55% Oxygen Loss |
| <img width="64" height="64" src="Assets/Oxygen_Tank.png"> | Oxygen Tank | Head | 8000 | -70% Oxygen Loss |

---

## Backpack and Belt Augments

**Important Notice, Please Read!**

**The current functionality of both backpack and belts have a small caveat to them due to the way these dynamic items are equipped in Raft. As a result, equipping lower-tier versions of these items while having items within those slots will cause those slots be hidden along with the item.**

**Your items will still be there if you re-equip the higher-tier items! Just make sure you do so and remove them if you plan on uninstalling the mod as this will cause those items in those slots to disappear.**

**That being said, the code for this will be refactored in the coming future in hopes to make it more friendly for the end-user.**

| Item Icon | Item Name | Property Augments |
| :---: | :---: | :---: |
| <img width="64" height="64" src="Assets/Duffel_Backpack.png"> | Duffle Backpack | +1 Additional Row (15 Backpack Slots Total) |
| <img width="64" height="64" src="Assets/Tactical_Backpack.png"> | Tactical Backpack | +2 Additional Rows (20 Backpack Slots Total) |
| <img width="64" height="64" src="Assets/Thatch_Belt.png"> | Thatch Belt | +1 Hotbar Slot / +1 Equipment Slot |
| <img width="64" height="64" src="Assets/Tattered_Belt.png"> | Tattered Belt | +2 Hotbar Slots / +2 Equipment Slots |
| <img width="64" height="64" src="Assets/Leather_Belt.png"> | Leather Belt | +3 Hotbar Slots / +3 Equipment Slots |
| <img width="64" height="64" src="Assets/Utility_Belt.png"> | Utility Belt | +4 Hotbar Slots / +4 Equipment Slots |
| <img width="64" height="64" src="Assets/Caravan_Belt.png"> | Caravan Belt | +5 Hotbar Slots / +5 Equipment Slots |

**The additional hotbar slots added do not have any keybinds associated with them. This may come in a later version.**

---

## Sprint/Walk Toggle

The mod also now comes with a Sprint/Walk Invert Toggle. Pressing `PageDown` will toggle between the two modes.

This means after pressing the key, your default behavior will Sprint and holding down sprint will make you Walk. Pressing the key again will revert you to the original behavior.

## Contact and Issues
Please contact me on Discord - Bahamut#2632 or the [Raft Modding Discord Server](https://discord.com/invite/Dwz7GSA)!