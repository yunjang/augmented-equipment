using System.Collections;
using System.Collections.Generic;

public static class OriginalEquipmentStats {
  public static Dictionary<string, Dictionary<string, float>> Properties = new Dictionary<string, Dictionary<string, float>> {
    ["ScrapHook"] = new Dictionary<string, float> {
      ["pullSpeed"] = 9f,
      ["gatherTime"] = 1f
    },
    ["TitaniumHook"] = new Dictionary<string, float> {
      ["pullSpeed"] = 6f,
      ["gatherTime"] = 0.5f
    },
    ["Player"] = new Dictionary<string, float> {
      ["normalSpeed"] = 3f,
      ["sprintSpeed"] = 5f,
      ["swimSpeed"] = 3f
    },
    ["Axe"] = new Dictionary<string, float> {
      ["useButtonCooldown"] = 1.1f
    },
    ["Head"] = new Dictionary<string, float> {
      ["oxygenLoss"] = 3f
    },
    ["Hotbar"] = new Dictionary<string, float> {
      ["width"] = 747f,
      ["height"] = 87f
    },
    ["Titanium_Axe"] = new Dictionary<string, float> {
      ["useButtonCooldown"] = 0.605f
    }
  };

  public static float Get(string propertyName, string valueName) {
    return Properties[propertyName][valueName];
  }
}
