# 1.3.2
- Fixed the ability to Shift + Click into an inactive hotbar slot

# 1.3.1
- Fixed a corner case with the `Belt` logic that caused issues with `Utility Belt`

# 1.3.0
- Researching base Backpack/Belt unlocks all higher-tier Backpacks/Belts
- Higher-tier backpacks now offset the Inventory UI for better visibility
- `Belt` Equipment Type Added
- Belts Added: `Thatch`, `Tattered`, `Leather`, `Utility`, `Caravan`
- Added in `Fortified Gloves`
- Fixed icon for `Oxygen Tank`
- All items moved to Weapons category to make use of its empty space
- All `Flipper` based items are now its own unique equipment type called `SwimFeet`

# 1.2.3
- Adding in some future compatibility with items from `AugmentedItems`
- Code refactored to be more consistent with `Network_Player`

# 1.2.2
- Added in compatibility with items from `AugmentedItems`

# 1.2.1
- Added `Flipper` to the custom dictionary of items to apply modifiers that overrides but replicates the same effect on vanilla
- Updated the dictionary of cached equipped items to explicitly contain keys while nulled out
  - This should resolve any sort of `KeyNotFoundExceptions`

# 1.2.0
- Code Refactored
  - All supporting code has been moved to respective files
  - `SetItem` patch has been overhauled to simply set modifiers rather than check for all equipped items and update modifiers
  - `CheckForEquipment` -> `CheckForEquipmentType`
    - Function is improved to check based on equipment slot type while obtaining and iterating through the modifiers in a more common code method
  - Custom logic to handle durability loss and more has been overhauled to no longer explicitly check Player Inventory
    - Now utilizes an `equipmentItemCache` data structure that is updated via `SetItem` patch
    - Significantly reduces any overhead of constant iteration of Player's Inventory and now does simple look-up
      - Should be `O(n)` down to `O(1)` now
- `Head` Equipment Support added
  - `Oxygen_Canister` and `Oxygen_Tank` added
- Sprint/Walk Invert Toggle Added
  - Pressing `PageDown` will now toggle the Player between Sprinting and Walking
    - e.g. Toggle to Sprint will cause default movement to sprint; pressing Sprint will cause you to Walk

# 1.1.0
- `Glove` Equipment Type Added
  - `Worn Gloves` Added
  - `Grip Gloves` Added
- Custom Backpacks Added
  - `PlayerInventory/InitializeSlots` patched to increase the number of Backpack slots originally available
  - `PlayerInventory/SetBackpackSlotState` patched to activate the slot states according to the backpack type
- `Axe/OnAxeHit` patched to allow durability reduction on gloves on tree chopping
- `PickupChanneling/TakeItemNetworked` patched to allow durability reduction on gloves on gather
- `PickupChanneling/ProgressChannel` patched to override the `pickupTime` to allow faster gathering
- `SetAxeButtonCooldown` added to update the `Axe` Controller whenever `Gloves` are equipped

# 1.0.0
- AugmentedEquipment Launched
