﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using HarmonyLib;
using static AugmentedEquipmentItems;
using static CustomEquipSlotType;
using static OriginalEquipmentStats;
using UltimateWater;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;

public class AugmentedEquipment : Mod {
  private Harmony harmony;
  private string instanceId = "com.bahamut.augmentedequipment";
  private AssetBundle asset;

  private List<Item_Base> addedItems = new List<Item_Base>();

  private static float globalGatherSpeedMultiplier = 0f;
  private static int maxHotbarSlotIndex = 9;
  private static int previousSlotIndex = 0;
  private static Vector3 originalInventoryVector = new Vector3(0f, 0f, 0f);
  private static Dictionary<EquipSlotType, string> equippedItemsCache = new Dictionary<EquipSlotType, string> {
    [CustomEquipSlotType.Get("None")] = null,
    [CustomEquipSlotType.Get("Feet")] = null,
    [CustomEquipSlotType.Get("SwimFeet")] = null,
    [CustomEquipSlotType.Get("Head")] = null,
    [CustomEquipSlotType.Get("Chest")] = null,
    [CustomEquipSlotType.Get("Backpack")] = null,
    [CustomEquipSlotType.Get("ZiplineTool")] = null,
    [CustomEquipSlotType.Get("Glove")] = null,
    [CustomEquipSlotType.Get("Belt")] = null
  };

  public IEnumerator Start() {
    AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(GetEmbeddedFileBytes("com.bahamut.assets"));
    yield return request;
    asset = request.assetBundle;

    LoadAugmentedEquipments();

    harmony = new Harmony(instanceId);
    harmony.PatchAll(Assembly.GetExecutingAssembly());
    Debug.Log("Mod AugmentedEquipment has been loaded!");
  }

  public void Update() {
    // If this key is hit, it will invert the player's movement properties.
    // Sprint speed will become walking speed; walking speed becomes sprinting speed.
    // Look into separating this functionality out of this mod and as a separate mod.
    if (Semih_Network.InMenuScene) {
      return;
    }
    if (Input.GetKeyDown(KeyCode.PageDown)) {
      InvertPlayerMovement();
    }
  }

  // Helper Methods
  private void LoadAugmentedEquipments() {
    foreach (KeyValuePair<string, ItemTemplate> kvp in AugmentedEquipmentItems.AllAugmentedEquipmentItems) {
      ItemTemplate itemTemplate = kvp.Value;
      if (itemTemplate.uniqueName != null) {
        CreateEquipment(
          itemTemplate.baseItemReference,
          itemTemplate.uniqueName,
          itemTemplate.displayName,
          itemTemplate.uniqueIndex,
          itemTemplate.description,
          itemTemplate.maxUses,
          itemTemplate.maxStacks,
          itemTemplate.equipSlotType,
          itemTemplate.subCategory,
          itemTemplate.subCategoryOrder,
          itemTemplate.recipe,
          itemTemplate.spriteName
        );
      } else if (kvp.Key.Equals("Flipper")) {
        // @TODO: Refactor ugly code here.
        Item_Base flipperItem = ItemManager.GetItemByName("Flipper");
        ItemInstance_Equipment equipmentSlotType = new ItemInstance_Equipment(CustomEquipSlotType.Get("SwimFeet"));
        Traverse.Create(flipperItem).Field("settings_equipment").SetValue(equipmentSlotType);
      }
    }
  }

  private void CreateEquipment(string baseItemName, string uniqueName, string displayName, int uniqueIndex, string description, int maxUses, int maxStacks, EquipSlotType slotType, string subCategory, int subCategoryOrder, Dictionary<string, int> recipe, string spriteName = null) {
    string spriteAsset = (spriteName != null) ? spriteName : uniqueName;
    Sprite augmentedItemSprite = asset.LoadAsset<Sprite>(spriteAsset);
    
    Item_Base baseEquipmentItem = ItemManager.GetItemByName(baseItemName);
    Item_Base newEquipmentItem = UnityEngine.Object.Instantiate(baseEquipmentItem);

    // Update the properties the item -- overriding the initialized property so we can set maxUses.
    if (baseItemName.Equals("Backpack")) {
      Traverse.Create(newEquipmentItem).Property("UniqueName").SetValue(uniqueName);
      Traverse.Create(newEquipmentItem).Property("UniqueIndex").SetValue(uniqueIndex);
      ItemInstance_Inventory itemInstanceInventory = new ItemInstance_Inventory(null, "", 1);
      Traverse.Create(newEquipmentItem).Field("settings_Inventory").SetValue(itemInstanceInventory);

    } else {
      Traverse.Create(newEquipmentItem).Field("hasBeenInitialized").SetValue(false);
      newEquipmentItem.Initialize(uniqueIndex, uniqueName, maxUses);
    }
    // ItemInstance_Inventory existingInventoryInstance = newEquipmentItem.settings_Inventory;
    ItemInstance_Inventory newInventoryInstance = new ItemInstance_Inventory(augmentedItemSprite, "", maxStacks);
    newEquipmentItem.settings_Inventory = newInventoryInstance;
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("DisplayName").SetValue(displayName);
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("Description").SetValue(description);
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("Sprite").SetValue(augmentedItemSprite);
    Traverse.Create(newEquipmentItem).Field("settings_Inventory").Property("stackSize").SetValue(maxStacks);
    ItemInstance_Equipment equipmentSlotType = new ItemInstance_Equipment(slotType);
    Traverse.Create(newEquipmentItem).Field("settings_equipment").SetValue(equipmentSlotType);

    ItemInstance_Recipe itemInstanceRecipe = new ItemInstance_Recipe(CraftingCategory.Weapons, false, false, subCategory, subCategoryOrder);
    newEquipmentItem.settings_recipe = itemInstanceRecipe;

    // Create the recipe for the item.
    List<CostMultiple> itemRecipe = new List<CostMultiple>();
    foreach (KeyValuePair<string, int> kvp in recipe) {
      Item_Base item = ItemManager.GetItemByName(kvp.Key);
      CostMultiple itemRecipeQuantity = new CostMultiple(new Item_Base[] { item }, kvp.Value);
      itemRecipe.Add(itemRecipeQuantity);
    }
    newEquipmentItem.settings_recipe.NewCost = itemRecipe.ToArray();
    addedItems.Add(newEquipmentItem);

    // Debug.Log($"Successfully Added {newEquipmentItem.UniqueName}");
    RAPI.RegisterItem(newEquipmentItem);
  }

  private static bool CheckForEquipmentType(PlayerInventory playerInventory, Dictionary<string, float> modifiers, string equipSlotTypeString) {
    EquipSlotType equipSlotType = CustomEquipSlotType.Get(equipSlotTypeString);
    Slot_Equip slotEquip = playerInventory.GetEquipmentSlotFromEquipmentType(equipSlotType);
    if (slotEquip != null) {
      ItemInstance itemInstance = slotEquip.itemInstance;
      string itemUniqueName = itemInstance.UniqueName;
      equippedItemsCache[equipSlotType] = itemUniqueName;

      ItemTemplate itemTemplate = null;
      AugmentedEquipmentItems.AllAugmentedEquipmentItems.TryGetValue(itemUniqueName, out itemTemplate);
      if (itemTemplate != null) {
        Dictionary<string, float> itemModifiers = itemTemplate.modifiers;
        if (itemModifiers != null) {
          foreach (KeyValuePair<string, float> kvp in itemModifiers) {
            modifiers[kvp.Key] += kvp.Value;
          }
        }
        return true;
      }
    }
    equippedItemsCache[equipSlotType] = null;
    return false;
  }

  private static GameObject GetHookGameObject(Network_Player networkPlayer, string hookType) {
    Transform playerRightHandTransform = networkPlayer.rightHandParent;
    foreach (Transform rightHandTransform in playerRightHandTransform) {
      Hook hookScript = rightHandTransform.gameObject.GetComponent<Hook>();
      if (hookScript != null) {
        GameObject hookGameObject = rightHandTransform.gameObject;
        foreach (Transform hookObjectTransform in rightHandTransform) {
          foreach (Transform hookModelTransform in hookObjectTransform) {
            if (hookModelTransform.gameObject.name.Contains("Model")) {
              MeshFilter meshFilter = hookModelTransform.gameObject.GetComponent<MeshFilter>();
              if (meshFilter.mesh.ToString().Contains(hookType)) {
                return hookGameObject;
              }
            }
          }
        }
      }
    }
    return null;
  }

  private static void SetAxeButtonCooldown(float axeButtonCooldownMultiplier) {
    Item_Base axeItemBase = ItemManager.GetItemByName("Axe");
    ItemInstance_Usable itemInstanceUsable = axeItemBase.settings_usable;
    Traverse.Create(itemInstanceUsable).Field("useButtonCooldown").SetValue(OriginalEquipmentStats.Get("Axe", "useButtonCooldown") - (OriginalEquipmentStats.Get("Axe", "useButtonCooldown") * axeButtonCooldownMultiplier));
  }

  private static bool IsBackpackEmpty(PlayerInventory inventory) {
    using (List<Slot>.Enumerator enumerator = inventory.backpackSlots.GetEnumerator()) {
      while (enumerator.MoveNext()) {
        if (!enumerator.Current.IsEmpty) {
          return false;
        }
      }
    }
    return true;
  }

  private void UnloadEquipment() {
    List<Item_Base> allItems = Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").GetValue() as List<Item_Base>;
    foreach (Item_Base item in addedItems) {
      // Debug.Log($"Unloading Item {item.UniqueName}...");
      allItems.Remove(item);
    }
    addedItems.Clear();
    Traverse.Create(typeof(ItemManager)).Field("allAvailableItems").SetValue(allItems);
  }

  private PersonController GetLocalPersonController() {
    Network_Player networkPlayer = RAPI.GetLocalPlayer();
    return networkPlayer.PersonController;
  }

  private void InvertPlayerMovement() {
    PersonController personController = GetLocalPersonController();
    float currentNormalSpeed = personController.normalSpeed;
    float currentSprintSpeed = personController.sprintSpeed;
    personController.normalSpeed = currentSprintSpeed;
    personController.sprintSpeed = currentNormalSpeed;
    float originalNormalSpeed = OriginalEquipmentStats.Get("Player", "normalSpeed");
    float originalSprintSpeed = OriginalEquipmentStats.Get("Player", "sprintSpeed"); 
    OriginalEquipmentStats.Properties["Player"]["normalSpeed"] = originalSprintSpeed;
    OriginalEquipmentStats.Properties["Player"]["sprintSpeed"] = originalNormalSpeed;
  }

  private static void UpdateBackpackUI() {
    Dictionary<string, int> backpackMap = new Dictionary<string, int> {
      ["Backpack"] = 0,
      ["Duffel_Backpack"] = 1,
      ["Tactical_Backpack"] = 2
    };

    Transform inventoryParent = GameObject.Find("InventoryParent").transform;
    Transform inventoryPlayer = inventoryParent.Find("Inventory_Player");
    string backpackItemCache = equippedItemsCache[CustomEquipSlotType.Get("Backpack")];
    int backpackTierMultiplier = (backpackItemCache != null) ? backpackMap[backpackItemCache] : 0;
    inventoryPlayer.localPosition = new Vector3(originalInventoryVector.x, originalInventoryVector.y + (35 * backpackTierMultiplier), 0);
  }

  private static void UpdatePlayerUI(bool isBeltEquipped) {
    Dictionary<string, int> beltMap = new Dictionary<string, int> {
      ["Thatch_Belt"] = 1,
      ["Tattered_Belt"] = 2,
      ["Leather_Belt"] = 3,
      ["Utility_Belt"] = 4,
      ["Caravan_Belt"] = 5
    };
    
    if (RAPI.GetLocalPlayer().IsLocalPlayer) {
      Transform inventoryParent = GameObject.Find("InventoryParent").transform;
      Transform hotbarParent = inventoryParent.Find("Hotbar");
      RectTransform hotbarParentRectTransform = hotbarParent.gameObject.GetComponent<RectTransform>();
      Rect hotbarParentRect = hotbarParentRectTransform.rect;
      string beltItemCache = equippedItemsCache[CustomEquipSlotType.Get("Belt")];
      int beltTierMultiplier = (beltItemCache != null) ? beltMap[beltItemCache] : 0;
      hotbarParentRectTransform.sizeDelta = new Vector2(OriginalEquipmentStats.Get("Hotbar", "width") + (70 * beltTierMultiplier), OriginalEquipmentStats.Get("Hotbar", "height"));
      Transform hotslotTransform = hotbarParent.Find("Hotslot parent");
      Slot[] hotSlots = hotslotTransform.GetComponentsInChildren<Slot>(true);
      for (int i = 10; i < 15; ++i) {
        hotSlots[i].gameObject.SetActiveSafe(false);
        hotSlots[i].active = false;
      }
      maxHotbarSlotIndex = 9;
      for (int i = 10; i < 10 + beltTierMultiplier; ++i) {
        hotSlots[i].gameObject.SetActiveSafe(true);
        hotSlots[i].active = true;
        maxHotbarSlotIndex = i;
      }
      Transform equipParent = inventoryParent.Find("Inventory_Player/SlotParent/Equipslot parent");
      GridLayoutGroup equipGridLayoutGroup = equipParent.gameObject.GetComponent<GridLayoutGroup>();
      Slot[] equipSlots = equipParent.GetComponentsInChildren<Slot>(true);
      for (int i = 5; i < 10; ++i) {
        equipSlots[i].gameObject.SetActiveSafe(false);
        equipSlots[i].active = false;
        equipGridLayoutGroup.constraintCount = 1;  
      }
      for (int i = 5; i < 5 + beltTierMultiplier; ++i) {
        equipSlots[i].gameObject.SetActiveSafe(true);
        equipSlots[i].active = true;
        equipGridLayoutGroup.constraintCount = 2;
      }
    }
  }

  // Patches
  [HarmonyPatch(typeof(Slot_Equip), "SetItem", new[] { typeof(ItemInstance) })]
  class SlotEquipSetItem_Patch {
    private static void Postfix(ItemInstance newInstance) {
      Network_Player networkPlayer = RAPI.GetLocalPlayer();
      PlayerInventory playerInventory = networkPlayer.Inventory;
      PersonController personController = networkPlayer.PersonController;
      PlayerStats playerStats = networkPlayer.Stats;

      GameObject scrapHook = GetHookGameObject(networkPlayer, "Scrap");
      Hook scrapHookScript = (scrapHook != null) ? scrapHook.GetComponent<Hook>() : null;
      GameObject titaniumHook = GetHookGameObject(networkPlayer, "Titanium");
      Hook titaniumHookScript = (titaniumHook != null) ? titaniumHook.GetComponent<Hook>() : null;

      Dictionary<string, float> modifiers = new Dictionary<string, float> {
        ["pullSpeedModifier"] = 0f,
        ["gatherTimeModifier"] = 0f,
        ["normalSpeedModifier"] = 0f,
        ["sprintSpeedModifier"] = 0f,
        ["swimSpeedModifier"] = 0f,
        ["axeButtonCooldownModifier"] = 0f,
        ["gatherSpeedModifier"] = 0f,
        ["oxygenLossModifier"] = 0f
      };

      bool isChestEquipped = CheckForEquipmentType(playerInventory, modifiers, "Chest");
      bool isFeetEquipped = CheckForEquipmentType(playerInventory, modifiers, "Feet");
      bool isSwimFeetEquipped = CheckForEquipmentType(playerInventory, modifiers, "SwimFeet");
      bool isGloveEquipped = CheckForEquipmentType(playerInventory, modifiers, "Glove");
      bool isBackpackEquipped = CheckForEquipmentType(playerInventory, modifiers, "Backpack");
      bool isHeadEquipped = CheckForEquipmentType(playerInventory, modifiers, "Head");
      bool isBeltEquipped = CheckForEquipmentType(playerInventory, modifiers, "Belt");

      if (isBackpackEquipped) {
        playerInventory.SetBackpackSlotState(true);
        UpdateBackpackUI();
      } else {
        playerInventory.SetBackpackSlotState(false);
        UpdateBackpackUI();
      }

      // Take the calculated modifiers and apply them to the Scrap Hook script.
      if (scrapHookScript != null) {
        scrapHookScript.pullSpeed = OriginalEquipmentStats.Get("ScrapHook", "pullSpeed") + (OriginalEquipmentStats.Get("ScrapHook", "pullSpeed") * modifiers["pullSpeedModifier"]);
        scrapHookScript.gatherTime = OriginalEquipmentStats.Get("ScrapHook", "gatherTime") - (OriginalEquipmentStats.Get("ScrapHook", "gatherTime") * modifiers["gatherTimeModifier"]);
      }
      if (titaniumHookScript != null) {
        titaniumHookScript.pullSpeed = OriginalEquipmentStats.Get("TitaniumHook", "pullSpeed") + (OriginalEquipmentStats.Get("TitaniumHook", "pullSpeed") * modifiers["pullSpeedModifier"]);
        titaniumHookScript.gatherTime = OriginalEquipmentStats.Get("TitaniumHook", "gatherTime") - (OriginalEquipmentStats.Get("TitaniumHook", "gatherTime") * modifiers["gatherTimeModifier"]);
      }

      // Take the calculated modifiers and apply them to the Player's Person Controller.
      personController.normalSpeed = OriginalEquipmentStats.Get("Player", "normalSpeed") + (OriginalEquipmentStats.Get("Player", "normalSpeed") * modifiers["normalSpeedModifier"]);
      personController.sprintSpeed = OriginalEquipmentStats.Get("Player", "sprintSpeed") + (OriginalEquipmentStats.Get("Player", "sprintSpeed") * modifiers["sprintSpeedModifier"]);
      personController.swimSpeed = OriginalEquipmentStats.Get("Player", "swimSpeed") + (OriginalEquipmentStats.Get("Player", "swimSpeed") * modifiers["swimSpeedModifier"]);

      // Take the calculated modifiers and apply them to the Player's Stats.
      playerStats.stat_oxygen.SetOxygenLostPerSecond(OriginalEquipmentStats.Get("Head", "oxygenLoss") - (OriginalEquipmentStats.Get("Head", "oxygenLoss") * modifiers["oxygenLossModifier"]));

      SetAxeButtonCooldown(modifiers["axeButtonCooldownModifier"]);
      globalGatherSpeedMultiplier = modifiers["gatherSpeedModifier"];

      UpdatePlayerUI(isBeltEquipped);
    }
  }

  [HarmonyPatch(typeof(Pickup), "PickupItem")]
  class PickupPickupItem_Patch {
    private static void Postfix(PickupItem pickup, bool forcePickup, ref bool triggerHandAnimation) {
      if (!forcePickup && !pickup.canBePickedUp) {
        return;
      }
      // If we have: gathered the item via Hook, Scrap Hook on hand, and a Pulley equipped, remove durability from the chestpiece.
      PlayerInventory playerInventory = RAPI.GetLocalPlayer().Inventory;
      if (playerInventory.GetSelectedHotbarItem() == null) {
        return;
      }
      string currentlySelectedItem = playerInventory.GetSelectedHotbarItem().UniqueName;
      if (forcePickup && !currentlySelectedItem.Equals("Hook_Plastic") && currentlySelectedItem.Contains("Hook_")) {
        bool isPulleyEquipped = (equippedItemsCache[CustomEquipSlotType.Get("Chest")] != null);
        if (isPulleyEquipped) {
          playerInventory.RemoveDurabillityFromEquipment(EquipSlotType.Chest);
        }
      }
    }
  }

  [HarmonyPatch(typeof(PersonController), "WaterControll")]
  class PersonControllerWaterControll_Patch {
    private static float movingTimer = 0f;
    private static float submersedTimer = 0f;
    private static void Postfix(ref bool ___moving, ref SubmersionState ___submersionState) {
      if (___moving) {
        movingTimer += Time.deltaTime;
        if (movingTimer >= 1f) {
          movingTimer = 0f;
          string equipmentFeetCache = equippedItemsCache[CustomEquipSlotType.Get("SwimFeet")];
          bool isFlipperEquipped = (equipmentFeetCache != null && equipmentFeetCache.Contains("Flippers"));
          if (isFlipperEquipped) {
            PlayerInventory playerInventory = RAPI.GetLocalPlayer().Inventory;
            Slot_Equip feetEquipmentSlot = playerInventory.GetEquipmentSlotFromEquipmentType(CustomEquipSlotType.Get("SwimFeet"));
            feetEquipmentSlot.IncrementUses(-1);
          }
        }
      }
      if (___submersionState == SubmersionState.Full) {
        submersedTimer += Time.deltaTime;
        if (submersedTimer >= 1f) {
          submersedTimer = 0f;
          string equipmentHeadCache = equippedItemsCache[CustomEquipSlotType.Get("Head")];
          bool isOxygenBottleEquipped = (equipmentHeadCache != null && equipmentHeadCache.Contains("Oxygen_"));
          if (isOxygenBottleEquipped) {
            PlayerInventory playerInventory = RAPI.GetLocalPlayer().Inventory;
            playerInventory.RemoveDurabillityFromEquipment(CustomEquipSlotType.Get("Head"));
          }
        }
      }
    }
  }

  [HarmonyPatch(typeof(PickupChanneling), "ProgressChannel")]
  class PickupChannelingProgressChannel_Patch {
    private static void Prefix(ref float ___pickupTime) {
      // @TODO: Not a huge deal at all since all this does is literally an assignment but figure out the one-stop shop to set the pickupTime.
      ___pickupTime = 1f - (1f * globalGatherSpeedMultiplier);
    }
  }

  [HarmonyPatch(typeof(PickupChanneling), "TakeItemNetworked")]
  class PickupChannelingTakeItemNetworked_Patch  {
    private static void Postfix(ref Network_Player ___player) {
      bool isGloveEquipped = equippedItemsCache[CustomEquipSlotType.Get("Glove")] != null;
      if (isGloveEquipped) {
        ___player.Inventory.RemoveDurabillityFromEquipment(CustomEquipSlotType.Get("Glove"));
      }
    }
  }

  [HarmonyPatch(typeof(Axe), "OnAxeHit")]
  class AxeOnAxeHit_Patch {
    private static void Postfix(ref Network_Player ___playerNetwork, ref RaycastHit ___chopHitPoint, ref LayerMask ___hitmask) {
      bool isGloveEquipped = equippedItemsCache[CustomEquipSlotType.Get("Glove")] != null;
      if (isGloveEquipped && Helper.HitAtCursor(out  ___chopHitPoint, 5f, ___hitmask)) {
        if (___chopHitPoint.transform.tag == "Tree") {
          ___playerNetwork.Inventory.RemoveDurabillityFromEquipment(CustomEquipSlotType.Get("Glove"));
        }
      }
    }
  }
    
  [HarmonyPatch(typeof(PlayerInventory), "InitializeSlots")]
  class PlayerInventoryInitializeSlots_Patch {
    private static void Prefix(PlayerInventory __instance) {
      // @TODO: Refactor this code to split out some of the code to be neater.
      GameObject hotbarParent = GameObject.Find("Hotbar/Hotslot parent");
      Slot[] allSlots = __instance.gridLayoutGroup.GetComponentsInChildren<Slot>();
      GridLayoutGroup[] gridLayoutGroups = __instance.gridLayoutGroup.GetComponentsInChildren<GridLayoutGroup>();
      GridLayoutGroup equipmentGroup = gridLayoutGroups[1];
      Slot regularSlot = null;
      int regularSlotStartingIndex = 0;
      Slot backpackSlot = null;
      int backpackSlotStartingIndex = 0;
      Slot equipmentSlot = null;
      Slot hotbarSlot = hotbarParent.transform.GetChild(0).GetComponent<Slot>();
      foreach(Slot slot in allSlots) {
        if (slot.slotType == SlotType.Normal) {
          regularSlot = slot;
          regularSlotStartingIndex++;
        }
        if (slot.slotType == SlotType.Backpack) {
          backpackSlot = slot;
          backpackSlotStartingIndex++;
        }
        if (slot.slotType == SlotType.Equipment) {
          equipmentSlot = slot;
        }
      }
      int hotbarSlotCount = 10;
      for (int i = 0; i < 5; ++i) {
        Slot clonedHotbarSlot = Instantiate(hotbarSlot);
        clonedHotbarSlot.transform.SetParent(hotbarParent.transform);
        clonedHotbarSlot.transform.localScale = new Vector3(1f, 1f, 1f);
        clonedHotbarSlot.transform.SetSiblingIndex(hotbarSlotCount);
        hotbarSlotCount++;
      }
      // Disabled for future use potentially.
      for (int i = 0; i < 0; ++i) {
        Slot clonedRegularSlot = Instantiate(regularSlot);
        clonedRegularSlot.transform.SetParent(__instance.gridLayoutGroup.transform);
        clonedRegularSlot.transform.localScale = new Vector3(1f, 1f, 1f);
        clonedRegularSlot.transform.SetSiblingIndex(regularSlotStartingIndex);
        regularSlotStartingIndex++;
      }
      backpackSlotStartingIndex += regularSlotStartingIndex;
      for (int i = 0; i < 10; ++i) {
        Slot clonedBackpackSlot = Instantiate(backpackSlot);
        clonedBackpackSlot.transform.SetParent(__instance.gridLayoutGroup.transform);
        clonedBackpackSlot.transform.localScale = new Vector3(1f, 1f, 1f);
        clonedBackpackSlot.transform.SetSiblingIndex(backpackSlotStartingIndex);
        backpackSlotStartingIndex++;
      }
      for (int i = 0; i < 5; ++i) {
        Slot clonedEquipmentSlot = Instantiate(equipmentSlot);
        clonedEquipmentSlot.transform.SetParent(equipmentGroup.transform);
        clonedEquipmentSlot.transform.localScale = new Vector3(1f, 1f, 1f);
      }
    }

    private static void Postfix(PlayerInventory __instance) {
      GameObject hotbarParent = GameObject.Find("Hotbar/Hotslot parent");
      GridLayoutGroup[] gridLayoutGroups = __instance.gridLayoutGroup.GetComponentsInChildren<GridLayoutGroup>();
      GridLayoutGroup equipmentGroup = gridLayoutGroups[1];
      Slot[] equipmentSlots = equipmentGroup.GetComponentsInChildren<Slot>();
      Slot[] hotbarSlots = hotbarParent.GetComponentsInChildren<Slot>();
      for (int i = 10; i < 15; ++i) {
        hotbarSlots[i].gameObject.SetActive(false);
        hotbarSlots[i].active = false;
      }
      for (int i = 5; i < 10; ++i) {
        equipmentSlots[i].gameObject.SetActiveSafe(false);
        equipmentSlots[i].active = false;
      }
      Transform inventoryParent = GameObject.Find("InventoryParent").transform;
      Transform inventoryPlayer = inventoryParent.Find("Inventory_Player");
      originalInventoryVector = new Vector3(inventoryPlayer.localPosition.x, inventoryPlayer.localPosition.y, 0f);
    }
  }

  [HarmonyPatch(typeof(Hotbar), "HandleHotbarSelection")]
  class HotbarHandleHotbarSelection_Patch {
    private static bool Prefix(Hotbar __instance, ref Network_Player ___playerNetwork, ref int ___slotIndex, ref RectTransform ___hotbarSelectionTransform) {
      if (!___playerNetwork.Inventory.GetSlot(___slotIndex).gameObject.activeSelf) {
        ___slotIndex = (previousSlotIndex > 0) ? 0 : maxHotbarSlotIndex;
        Slot updatedSlot = ___playerNetwork.Inventory.GetSlot(___slotIndex);
          ___hotbarSelectionTransform.localPosition = updatedSlot.rectTransform.localPosition;
          __instance.SelectHotslot(updatedSlot);
        return false;
      }
      previousSlotIndex = ___slotIndex;
      return true;
    }
  }

  [HarmonyPatch(typeof(PlayerInventory), "SetBackpackSlotState")]
  class PlayerInventorySetBackpackSlotState_Patch {
    private static void Postfix(PlayerInventory __instance, bool state) {
      // If the state is false, then ignore this function. Otherwise, get the number of backpack slots to set active.
      int maxBackpackSlots = 10;
      if (state) {
        PlayerInventory inventory = RAPI.GetLocalPlayer().Inventory;
        string cachedBackpack = equippedItemsCache[CustomEquipSlotType.Get("Backpack")];
        if (cachedBackpack != null) {
          if (cachedBackpack.Equals("Duffel_Backpack")) {
            maxBackpackSlots = 15;
          } else if (cachedBackpack.Equals("Tactical_Backpack")) {
            maxBackpackSlots = 20;
          }
        }
      }

      if (state) {
        int activatedBackpackSlots = 0;
        foreach (Slot slot in __instance.allSlots) {
          if (slot.slotType == SlotType.Backpack && activatedBackpackSlots < maxBackpackSlots) {
            slot.gameObject.SetActiveSafe(state);
            slot.active = state;
            activatedBackpackSlots++;
          } else if (slot.slotType == SlotType.Backpack) {
            slot.gameObject.SetActiveSafe(!state);
            slot.active = !state;
          }
        }
      }
    }
  }

  [HarmonyPatch(typeof(Inventory_ResearchTable), "LearnItem")]
  class Inventory_ResearchTableLearnItem_Patch {
    private static void Postfix(Inventory_ResearchTable __instance, Item_Base item, CSteamID researcherID, ref List<ResearchMenuItem> ___menuItems, ref string ___eventRef_Learn, ref NotificationManager ___notificationManager) {
      if (item.UniqueName.Contains("Backpack")) {
        // If we learn a backpack, then we want to learn all of the backpack tiers -- for the sake of not forcing players into insane material sink.
        Item_Base[] allBackpacks = ItemManager.GetAllItems().Where(backpack => backpack.UniqueName.Contains("_Backpack")).ToArray();
        for (int i = 0; i < ___menuItems.Count; ++i) {
          foreach (Item_Base backpackItem in allBackpacks) {
            if (___menuItems[i].GetItem().UniqueIndex == backpackItem.UniqueIndex) {
              (___notificationManager.ShowNotification("Research") as Notification_Research).researchInfoQue.Enqueue(new Notification_Research_Info(item.settings_Inventory.DisplayName, researcherID, item.settings_Inventory.Sprite));
              ___menuItems[i].Learn();
            }
          }
        }
      }
      if (item.UniqueName.Contains("Belt")) {
        Item_Base[] allBelts = ItemManager.GetAllItems().Where(belt => belt.UniqueName.Contains("_Belt")).ToArray();
        for (int i = 0; i < ___menuItems.Count; ++i) {
          foreach (Item_Base beltItem in allBelts) {
            if (___menuItems[i].GetItem().UniqueIndex == beltItem.UniqueIndex) {
              (___notificationManager.ShowNotification("Research") as Notification_Research).researchInfoQue.Enqueue(new Notification_Research_Info(item.settings_Inventory.DisplayName, researcherID, item.settings_Inventory.Sprite));
              ___menuItems[i].Learn();
            }
          }
        }
      }
    }
  }

  [HarmonyPatch(typeof(Inventory_ResearchTable), "OnWorldRecievedLate")]
  class Inventory_ResearchTableOnWorldRecievedLate_Patch {
    private static void Postfix(Inventory_ResearchTable __instance) {
      if (Semih_Network.IsHost) {
        List<ResearchMenuItem> menuItems = __instance.GetMenuItems();
        bool backpackAlreadyResearched = menuItems.Exists(menuItem => (menuItem.GetItem().UniqueName.Contains("Backpack") && menuItem.Learned));
        if (backpackAlreadyResearched) {
          Item_Base[] allBackpacks = ItemManager.GetAllItems().Where(backpack => backpack.UniqueName.Contains("_Backpack")).ToArray();
          for (int i = 0; i < menuItems.Count; ++i) {
            foreach (Item_Base backpackItem in allBackpacks) {
              if (menuItems[i].GetItem().UniqueIndex == backpackItem.UniqueIndex) {
                menuItems[i].Learn();
              }
            }
          }
        }
        bool beltAlreadyResearched = menuItems.Exists(menuItem => (menuItem.GetItem().UniqueName.Contains("Belt") && menuItem.Learned));
        if (beltAlreadyResearched) {
          Item_Base[] allBelts = ItemManager.GetAllItems().Where(backpack => backpack.UniqueName.Contains("_Belt")).ToArray();
          for (int i = 0; i < menuItems.Count; ++i) {
            foreach (Item_Base beltItem in allBelts) {
              if (menuItems[i].GetItem().UniqueIndex == beltItem.UniqueIndex) {
                menuItems[i].Learn();
              }
            }
          }
        }
      }
    }
  }

  [HarmonyPatch(typeof(PlayerInventory), "FindSuitableSlot")]
  class PlayerInventoryFindSuitableSlot_Patch {
    private static bool Prefix(ref Slot __result, ref List<Slot> ___allSlots, int startSlotIndex, int endSlotIndex, Item_Base stackableItem = null) {
      // Native function that is copied from PlayerInventory to ensure that the checks are done according to the game and tweak the results based on slot's active state.
      bool flag = stackableItem != null && stackableItem.settings_Inventory.Stackable;
      Slot slot = null;
      for (int i = startSlotIndex; i < endSlotIndex; i++)
      {
        if (i >= 0 && i < ___allSlots.Count)
        {
          Slot slot2 = ___allSlots[i];
          if (slot2.IsEmpty && slot == null)
          {
            slot = slot2;
          }
          if (flag && !slot2.StackIsFull() && !slot2.IsEmpty && slot2.itemInstance.UniqueIndex == stackableItem.UniqueIndex)
          {
            if (!slot2.active) {
              __result = null;
              return false;
            }
          }
        }
      }
      if (!slot.active) {
        __result = null;
        return false;
      }
      return true;
    }
  }

  public void OnModUnload() {
    harmony.UnpatchAll(instanceId);
    UnloadEquipment();
    asset.Unload(true);
    Debug.Log("Mod AugmentedEquipment has been unloaded!");
  }
}
