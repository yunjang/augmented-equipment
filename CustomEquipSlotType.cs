using System.Collections;
using System.Collections.Generic;

public static class CustomEquipSlotType {
  static Dictionary<string, EquipSlotType> EquipSlotDictionary = new Dictionary<string, EquipSlotType> {
    ["None"] = (EquipSlotType) 0,
    ["Feet"] = (EquipSlotType) 1,
    ["Head"] = (EquipSlotType) 2,
    ["Chest"] = (EquipSlotType) 4,
    ["Backpack"] = (EquipSlotType) 6,
    ["ZiplineTool"] = (EquipSlotType) 7,
    ["Glove"] = (EquipSlotType) 8,
    ["Belt"] = (EquipSlotType) 9,
    ["SwimFeet"] = (EquipSlotType) 10
  };

  public static EquipSlotType Get(string equipmentType) {
    return EquipSlotDictionary[equipmentType];
  }
}
